#!/usr/bin/env python

import threading
import time
import signal
import anydbm
import socket
import os
import urllib
import sys

from geopy import Point
from geopy.distance import vincenty

# for call station
import sqlite3

#radio module in the local dir
import si4703

from optparse import OptionParser

try:
    # so we can run this outside of kodi
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except ImportError:
    no_kodi=True

# This is so we can run it without kodi

try:
    __addon__       = xbmcaddon.Addon(id='script.service.radiofm')
    __addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )
except:
    __addondir__ = "/home/pi/.kodi/userdata/addon_data/script.service.radiofm/"

rds_thread=True

TCP_IP="0.0.0.0"
# picked the chipset of the radio
TCP_PORT=4703
BUFFER_SIZE=1024

variables={}

STATION_DB=__addondir__+"station.db"

def log(logline):
    print "RADIOFM: " + logline

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=value
        radioFM.send_network(radiofm, "set_kodi_prop " +  property + " " + strvalue+"\n")
    else: 
        xbmcgui.Window(10000).setProperty(property, strvalue)

# Only used if doing the TCP IP stuff. I use this function (set_kodi_prop) too much
# to change the definition
def set_kodi_prop_lcl(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    if no_kodi==True:
        variables[property]=value
    else: 
        xbmcgui.Window(10000).setProperty(property, strvalue)
        
def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
            radioFM.send_network(radiofm, "get_kodi_prop " +  property + "\n")
        ### Exception - because it's really annoying seeing once a second        
    else:
        value=xbmcgui.Window(10000).getProperty(property)
    if property!='radio_active':
        log("Loading: '" + property + "', '" + value + "'")        
    return value        

def signal_handler(signal, frame):
    global rds_thread
    rds_thread=False

    
class radioFM():
        
    fm = None
    # This is going to be used to store information about a station so that we can determine
    # If we should bother with RDS or not.
    # We'll .clear this later. Maybe based on major location change    
    station_dict={}
    
    # call station
    station_call={}
    
    # full scan flag.  If we want to do a full scan we can use this data later for stuff like
    # pty groups, When this is true RDS will not run because the full scan will be all consuming
    full_scan=False

    # This is going to be used to sync up various things. Mostly, I don't want to do a full 
    # scan and the regular RDS scan at the same time.  There may be other conditions
    # where I don't want to do something if we're in the middle of an RDS poll.
    polling_rds0a=False
    polling_rds2a=False    
    
    # Keep track of this.. because if we are updating we'll skill certain operations
    updating_db=False

    db_path=__addondir__+"FM.db"

    #socket 
    sock=None
    socket_connect=False
    base_seekth=0
    
    
    
    def __init__(self):

        # Initialize the call database.
        log ("Initializing database.")
        self.init_db()

        # Initialize radio parameters        
        log("Initializing radio")
        self.fm = si4703.si4703()
        self.fm.init()

    def start(self):

        # we have to start the network first because of communication purposes.
        log ("Starting network")
        threading.Thread(target=self.start_network).start()
        
        # wait until the network is established
        
        while(self.sock==None):
            time.sleep(1)
            
#        set_kodi_prop("radio_active", "1")    
    # For testing 
#    	set_kodi_prop("gps_status", "active")

        # start muted (init disabled muting)
        # at this point we have no idea what the primary audio source is, so we'll play it safe
        
        #        self.fm.toggle_mute()
        last_tune=self.get_last_tune()
        self.tune(last_tune)
        # This is the default SEEKTH from the config
        base_seekth=self.fm.SYS_CONFIG_2.SEEKTH
        set_kodi_prop("radio_base_seekth", str(base_seekth))
        
        last_seekth=int(self.get_last_seekth())
        self.fm.SYS_CONFIG_2.SEEKTH=last_seekth
        self.fm.write_registry()
        
        # rds groups 0a and 2s
        threading.Thread(target=self.do_group0a).start()
        threading.Thread(target=self.do_group2a).start()        

    def network_response(self,data):
        tune=False
        data_split=data.split("\n")
        # loop through the data
        for data_line in data_split:
            request=data_line.split(" ")
            if request[0]=="TMUTE":
                self.fm.toggle_mute()
            if request[0]=="TUNE":
                tune_str=request[1]
                tune=True
            if request[0]=="SEEKTH":
                seekth_str=request[1]
                # save the setting to the chip
                self.fm.SYS_CONFIG_2.SEEKTH = int(seekth_str)
                self.fm.write_registry()
                self.store_seekth(seekth_str)
            if request[0]=="set_kodi_prop":
                if len(request)<3:
                    return
                set_kodi_prop_lcl(request[1], " ".join(request[2:]))

        if tune==True:
            self.tune(tune_str)
        
    def start_network(self):
        # This is the network client handler
        # Typically, we pass data back and forth using kodi variables.
        # But in this case, this process has no way to "wake" up and listen for those changes.
        # (like tuning)
        # So, by accepting network commands it will change stations, etc
        # This makes development easier as well, because the server can be running on the pi
        # while the GUI is developed in a VM, which is much quicker
        # The network server is part of the mananger service
        server_address = (TCP_IP, TCP_PORT)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        while self.socket_connect==False:
            try:
                self.sock.connect(server_address)
                self.socket_connect=True
            except:
                time.sleep(1)
        
        
        # Tells the manager that we're here and listening so it can get the radio socket
        self.send_network("RADIO SERVICE")
                
        while True:
            data = self.sock.recv(BUFFER_SIZE)
            if not data:
                log("Closing socket")
                self.sock.close()
                return
            self.network_response(data)

    def send_network(self, data):
        self.sock.send(data)
        
    def tune(self, tune_str):
        # This is set up so that it will tune a specific channel/
        # However, in the socket functions above there is a possibility
        # to seek.  My Frontend application is going to do this manually.
        # But I left it there. So if tune_str is none, then it assumed
        # that tuning has occured outside of this function
        if tune_str!=None:
            self.fm.tune(tune_str)
            # reset the memory for polling RDS
            self.fm.station_ps[tune_str]=1
            self.fm.station_rt[tune_str]=1


        # wait for tuning to complete
        self.fm.wait_for_tune()
        
        freq=str(self.fm.get_channel())
        set_kodi_prop("radio_ps", "No data")
        set_kodi_prop("radio_text", "No data")
        set_kodi_prop("radio_freq", freq)
        set_kodi_prop("radio_rssi", str(self.fm.STATUS_RSSI.RSS))
        set_kodi_prop("radio_seekth", str(self.fm.SYS_CONFIG_2.SEEKTH))
        ## I want to thread this for a few reasons.
        # I don't want to wait for it, in case it takes awhile
        # I want the station to be looked up, even if we're scanning
        # I don't want to look it up later, or worry about it later
        # The GUI will just update when it's done
       
        # Update: After implementing this, it turns out that it is incredibly
        # fast.  But I'm going to leave at as a seperate thread because of sqllite
        threading.Thread(target=self.lookup_call, args=(freq,)).start()
        
        # And finally, we are going to store the last tuned station to anydbm
        # so that when this starts it tunes to the last known station.
        self.store_tune(freq)
        
    def get_last_tune(self):
        try:
            db=anydbm.open(STATION_DB, "r")
            last_tune=db["last_tune"]
        except:
            # DB doesn't exist or something
            last_tune=88.1
        return last_tune

    def get_last_seekth(self):
        try:
            db=anydbm.open(STATION_DB, "r")
            last_seekth=db["last_seekth"]
        except:
            # DB doesn't exist or something
            last_seekth=self.fm.SYS_CONFIG_2.SEEKTH
        return last_seekth
    
    def store_tune(self, freq):
        db=anydbm.open(STATION_DB, "c")
        db["last_tune"]=freq
        db.close

    def store_seekth(self, seekth):
        db=anydbm.open(STATION_DB, "c")
        db["last_seekth"]=seekth
        db.close
        
    def end(self):
        global rds_thread
        log("ENDING.")
        rds_thread=False
        
    def purge_db(self):
        log("Cleaning up database.")
        db_conn=sqlite3.connect(self.db_path)        
        db_conn.execute("drop table if exists last_update")
        db_conn.execute("drop table if exists FM_data")
        db_conn.commit()
        db_conn.close()
        
    def init_db(self):
        
        if not os.path.exists(__addondir__):
            os.makedirs(__addondir__)

        log("Connecting to " + self.db_path)        
        db_conn=sqlite3.connect(self.db_path)
        # check for the tables existing. Part if the initialization process
        db_conn.execute("create table if not exists last_update(upd_time date)")
        db_conn.execute("create table if not exists FM_data (\
        	ins_date date, \
        	callsign varchar(8), \
            frequency varchar(5), \
            city varchar(128), \
            state varchar(2), \
            longitude decimal(10,6), \
            latitude decimal(10,6))")
        db_conn.execute("create index if not exists freq_idx on FM_data(frequency)")
        db_conn.execute("create index if not exists ins_idx on FM_data(ins_date)")
        db_conn.commit()
        # This will be reusable, because we will want to call this again sometime
        db_conn.close()
        
        # update the DB, if needed
        self.update_database()

    def update_database(self):
        # more likely than not, an update will not occur
        update=False
        
        # Check if the database needs to be updated, and if we're online update it
        # If we're not.. can we queue the update?
        # We may have all sorts of stuff queued up.. I'm not sure if we should
        # attack if we're online. This is not the most critcal thing to update either
        # so maybe if we're online for 5 full minutes or something.
        # We'll also need to make sure that we can handle a failure properly.
        
        db_conn=sqlite3.connect(self.db_path)
        cursor=db_conn.cursor()
        
        # This will be blank if the time diff is > 30 days or so
        cursor.execute("select upd_time from last_update where upd_time >= date('now', '-30 days');")
        row=cursor.fetchone()
        if row==None: # Doesn't return anything then it's blank or > 30 days old
            update=True
            
        if update==True:
            # Set the flag so we can use this in other threads
            self.updating_db=True
            try:
                log("Pulling data to CSV...")
                urllib.urlretrieve ("https://transition.fcc.gov/fcc-bin/fmq?call=&arn=&state=&city=&freq=0.0&fre2=107.9&serv=FM&vac=3&facid=&asrn=&class=&list=4&ThisTab=Results+to+This+Page%2FTab&dist=&dlat2=&mlat2=&slat2=&NS=N&dlon2=&mlon2=&slon2=&EW=W&size=9", __addondir__+ "fmquery.csv")
            except :
                # We'll say we're done, but we will not succeed.
                update=False
                self.updating_db=False
                log ("Getting FCC file has failed.")
                return
            try:
                log("Processing data...")
                FM=open(__addondir__+ "fmquery.csv")
                for line in FM:
                    # empty lines?
                    if len(line)<10:
                        continue
                    fields=line.split('|')
                    # The fields we want:
                    #2, the call sign
                    callsign=fields[1].rstrip()
                    #3 the frequency, but drop the hertz
                    frequency=fields[2].split(' ')[0]
                    #11 city
                    city=fields[10].rstrip()
                    #12 state
                    state=fields[11].rstrip()
                    # Long 21-24
                    # lat 25-28
                    lat=fields[20].rstrip()+" "+fields[21].rstrip()+"m "+fields[22].rstrip()+"s "+fields[19]
                    long=fields[24].rstrip()+" "+fields[25].rstrip()+"m "+fields[26].rstrip()+"s "+fields[23]
                    p=Point(lat + long)
                    db_conn.execute("insert into FM_data values(date('now'), ?, ?, ?, ?, ?, ?)",
                    	(callsign,frequency,city,state,p.latitude,p.longitude,))

                # delete the old data, but keep the new. Safety
                db_conn.execute("delete from FM_data where ins_date < date('now')")
                db_conn.execute("delete from last_update")
                db_conn.execute("insert into last_update values(date('now'))")
                log("Comitting...")
                db_conn.commit()
                self.updating_db=False
                log("Done..")

                os.remove(__addondir__+ "fmquery.csv")
            except:
                log("There was an error processing the FCC file.")
                print sys.exc_info()[0]
                raise
            db_conn.close()

    def lookup_call(self, freq):
        # If the GPS is not active then we're not returning anything
        mode=get_kodi_prop("gps_mode")
        if mode=="":
            set_kodi_prop("radio_callsign", "")
            set_kodi_prop("radio_city", "")
            set_kodi_prop("radio_state", "")
            set_kodi_prop("radio_miles", "")
            return
            
        mode=int(mode)
        if mode<2:
            return
        
        db_conn=sqlite3.connect(self.db_path)
        cursor=db_conn.cursor()
        
        # Placeholders
        lat=float(get_kodi_prop("gps_lat"))
        long=float(get_kodi_prop("gps_lon"))
        my_loc=(lat,long)
        tower_row=None
        # Make this pretty large
        miles=200
        for row in cursor.execute("select * from FM_data where frequency=?", (freq,)):
            tower_loc=(float(row[5]), float(row[6]))
            test_miles = vincenty(my_loc, tower_loc).miles
            if test_miles<miles:
                miles=test_miles
                tower_row=row
            # we didn't find anytihng
        if tower_row==None:
            set_kodi_prop("radio_callsign", "")
            set_kodi_prop("radio_city", "")
            set_kodi_prop("radio_state", "")
            set_kodi_prop("radio_miles", "")
        else:
            set_kodi_prop("radio_callsign", tower_row[1])
            set_kodi_prop("radio_city", tower_row[3])
            set_kodi_prop("radio_state", tower_row[4])
            set_kodi_prop("radio_miles", str(int(miles)))
            
        return

    def do_group0a(self):
        global rds_thread
        while (1):
            # I want to know if we are stero or not, and the current signal
            # These things are always changing.
            set_kodi_prop("radio_rssi", str(self.fm.STATUS_RSSI.RSS))
            set_kodi_prop("radio_st", str(self.fm.STATUS_RSSI.ST))
            set_kodi_prop("radio_seekth", str(self.fm.SYS_CONFIG_2.SEEKTH))            
#            log("In 0A")
            # default to enabled
            poll_rds=1
            
            # if we are doing a full radio scan do not let this function do anything
            if self.full_scan==True:
                sleep(5)
                continue
            # only do RDS if the radio frontend is enabled 
            # This service will run all the time, so only do this if the GUI is loaded
            if get_kodi_prop("radio_active")=="1":
                freq=str(self.fm.get_channel())
                # check if the station even is handing this out
                # But this is reset on tune, because signal change                
                if freq in self.fm.station_ps:
                    poll_rds=self.fm.station_ps[freq]
                    
                if poll_rds==1:
                    self.polling_rds0a=True
                    self.fm.read_group0a(3)
                    self.polling_rds0a=False
                    set_kodi_prop("radio_ps", self.fm.PS)
                    set_kodi_prop("radio_pty", self.fm.pty)
#                    log("Tuning: " + self.fm.PS)
#                    log("Type  : " + self.fm.pty)
                else:
                    log("Not polling 0A for " + freq)
                    
            if rds_thread==False:
                return
            time.sleep(2)

    def do_group2a(self):
        global rds_thread
        while (1):
#            log("In 2A")            
            # default to enabled
            poll_rds=1
            # if we are doing a full radio scan do not let this function do anything
            if self.full_scan==True:
                sleep(5)
                continue
            # only do RDS if the radio frontend is enabled 
            # This service will run all the time, so only do this if the GUI is loaded
            if get_kodi_prop("radio_active")=="1":
                freq=str(self.fm.get_channel())
                # check if the station even is handing this out
                # But this is reset on tune, because signal change
                if freq in self.fm.station_rt:
                    poll_rds=self.fm.station_rt[freq]
                    
                if poll_rds==1:
                    self.polling_rds2a=True
                    self.fm.read_group2a(3)
                    self.polling_rds2a=False
                    set_kodi_prop("radio_text", self.fm.RadioText.rstrip())
#                    log("Radiotext: " + self.fm.RadioText)
                else:
                    log("Not polling 2A for " + freq)
                    
            if rds_thread==False:
                return
            time.sleep(2)
            
def kodi_mon():
    global rds_thread    
    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            radioFM.end(radiofm)
            rds_thread=False
            break

if __name__ == "__main__":
    global radiofm

    log("Starting up")
    
    if no_kodi==False:
        threading.Thread(target=kodi_mon).start()
    else:
        signal.signal(signal.SIGINT, signal_handler)

        
    radiofm = radioFM()
    radiofm.start()
